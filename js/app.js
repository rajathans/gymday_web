let map;
let API_URL = 'http://127.0.0.1:8000/api/';
let currentLocation = {
	lat: '',
	lng: ''
};

let endLocation = {
	lat: '',
	lng: ''
};

$(document).ready(function () {
	fetch(API_URL + 'lists')
		.then(function (data) {
			return data.json();
		})
		.then(function (response) {
			orders = response.data;
			console.log(orders);

			if (orders.length) {
				let html = '';

				completedOrders = 0;
				$.each(orders, function (key, value) {
					if (value.isActive == 'yes') {
						setCurrentOrder(value.id - 1);
					}

					completedOrders += value.status == 'FINISHED_DELIVERY';

					html +=
						'<tr id="' + value.id + '" value="' + value.id + '">' +
						'<td>' + value.id + '</td>' +
						'<td>' + value.endLocation + '</td>' +
						'<td id="eta-' + value.id + '">' + ((value.isActive != 'no') ? value.eta : '-') + '</td>' +
						'<td><a class="btn btn-light ' + ((value.isActive != 'no') ? '"' : 'disabled"') + ' onclick="finish(' + value.id + ')">' + (value.status == 'FINISHED_DELIVERY' ? 'Finished' : 'Finish') + '</a></td></tr>';
				});

				if (completedOrders == 3) {
					$("#map").html('<h3 style="text-align:center">All orders delivered</h3>')
				} else {
					populateMap();
				}

				$("#orders-table").html(html);
			}
		});
});

function getCoordinates(address) {
	fetch('https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyC0JL-oiVAyUxvkaemN40PKnXqMlSLzsTY&address=' + encodeURI(address))
		.then(function (data) {
			console.log(data);
		});
}

function finish(id) {
	fetch(API_URL + 'orders/' + getCurrentOrder() + '/finish', {
			method: 'PUT',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			credentials: 'same-origin'
		})
		.then(function (data) {
			return data.json();
		})
		.then(function (response) {
			if (id < 3) {
				setCurrentOrder(id);
				location.reload();
			} else {
				location.reload();
			}
		});
}

function populateMap() {
	currentOrder = getCurrentOrder();

	if (currentOrder) {
		fetch(API_URL + 'orders/' + currentOrder)
			.then(function (data) {
				return data.json();
			})
			.then(function (response) {
				// Update map here
				orderData = response.data.order;
				endLocation = {
					lat: parseFloat(orderData.endLat),
					lng: parseFloat(orderData.endLng)
				};

				map = new google.maps.Map(document.getElementById('map'), {
					center: endLocation,
					zoom: 12
				});

				google.maps.event.addListener(map, 'click', function (event) {
					placeMarker({
						lat: parseFloat(event.latLng.lat()),
						lng: parseFloat(event.latLng.lng())
					});
				});

				placeMarker(endLocation, true);

				$.each(JSON.parse(orderData.checkpoints), function (key, value) {
					placeMarker(value, true);
				});
			});
	}
}

function getCurrentOrder() {
	return localStorage.getItem('currentOrder');
}

function setCurrentOrder(id) {
	localStorage.setItem('currentOrder', id + 1);
}

function placeMarker(location, doNotAdd = false) {
	console.log(location);
	console.log(endLocation);
	if (location == endLocation) {
		new google.maps.Marker({
			position: location,
			map: map,
			icon: {
				path: google.maps.SymbolPath.CIRCLE,
				scale: 10
			}
		});
	} else {
		new google.maps.Marker({
			position: location,
			map: map
		});
	}

	currentLocation = {
		lat: location.lat,
		lng: location.lng
	};

	if (!doNotAdd) {
		addCheckpoint(currentLocation);
		getEta();
	}
}

function addCheckpoint(coordinates) {
	console.log(coordinates);
	fetch(API_URL + 'orders/' + getCurrentOrder() + '/checkpoint', {
			method: 'PUT',
			body: JSON.stringify(coordinates),
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			credentials: 'same-origin'
		})
		.then(function (data) {
			return data.json();
		})
		.then(function (response) {

		});
}

function getEta() {
	var service = new google.maps.DistanceMatrixService();
	service.getDistanceMatrix({
		origins: [new google.maps.LatLng(currentLocation.lat, currentLocation.lng)],
		destinations: [new google.maps.LatLng(endLocation.lat, endLocation.lng)],
		travelMode: 'DRIVING'
	}, function (response, status) {
		console.log(response);
		if (status == 'OK') {
			eta = response.rows[0].elements[0].duration.text;
			$("#eta-" + getCurrentOrder()).html(eta);
			updateEta(eta);
		}
	});
}

function updateEta(eta) {
	fetch(API_URL + 'orders/' + getCurrentOrder() + '/eta', {
			method: 'PUT',
			body: JSON.stringify({
				eta: eta
			}),
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			credentials: 'same-origin'
		})
		.then(function (data) {
			return data.json();
		})
		.then(function (response) {

		});
}

function createNewList() {
	fetch(API_URL + 'lists', {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			credentials: 'same-origin'
		})
		.then(function (data) {
			return data.json();
		})
		.then(function (response) {
			location.reload();
		});
}